FROM python

# === Stuff that will not change =======================================================

# Install Xvfb to create virtual display
RUN apt update -qq && apt install xvfb -yqq && rm -rf /var/lib/apt/lists/*

# Install Google Chrome
RUN wget -q https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    dpkg -i --force-depends google-chrome-stable_current_amd64.deb || true

# Install missing dependencies if any due to dpkg usage
RUN apt update -qq && apt install -fyqq && rm -rf /var/lib/apt/lists/*

# === Stuff that might change ==========================================================

# Set environmental variables
ENV WORKDIR="/app"
ENV XVFB_PORT=99
ENV VIRTUAL_DISPLAY_SIZE=1920x1080x16
ENV TIMEOUT_AFTER_XVFB_START=5

# Need this to have Xvfb working correctly
ENV DISPLAY=:$XVFB_PORT

# Need this to have chromedriver working correctly
ENV PATH="$WORKDIR:$PATH"

# Set working directory for image
WORKDIR $WORKDIR

# === Stuff that will change sometimes =================================================

# Set up the environment
COPY requirements.txt .
RUN pip install -r requirements.txt

# === Stuff that will change ===========================================================

# Copy the source code to the image. Use .dockerignore to skip unimportant files!
COPY . .

# === Commands to be executed after docker run =========================================

CMD Xvfb -ac :$XVFB_PORT -screen 0 $VIRTUAL_DISPLAY_SIZE & \
    echo "Please stand by..." && \
    sleep $TIMEOUT_AFTER_XVFB_START && \
    clear && \
    pytest -v
