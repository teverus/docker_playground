import os
import random
import string
from time import sleep

from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


def test_1(driver):
    url = os.environ.get("URL", "https://www.google.com/")
    user_input = "(//*[@role='search']//input)[1]"
    driver.get(url)
    wait = WebDriverWait(driver, timeout=10)
    # wait.until(lambda _: url in driver.current_url)
    # sleep(3)
    wait.until(EC.visibility_of_element_located((By.XPATH, user_input)))
    search = driver.find_element(By.XPATH, user_input)
    search.send_keys(''.join(random.choice(string.digits) for _ in range(5)))
    search.send_keys(Keys.ENTER)
    try:
        wait.until(lambda _: driver.current_url != url)
    except Exception:
        raise Exception(
            f"\ncurrent  = {driver.current_url}"
            f"\nexcepted = {url}"
        )

    driver.save_screenshot("hello_world_666.png")