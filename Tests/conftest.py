'''import os
import platform

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture(autouse=True)
def driver():
    """Creates an instance of a driver that will be used in the tests"""

    # Make WebDriver save a driver executable in the root directory of the project
    os.environ["WDM_LOCAL"] = "1"

    # Start a driver with the desired options
    options = Options()
    options.headless = True
    options.add_argument("--window-size=1920,1080")  # Must match virtual display size
    options.add_argument("--lang=en")  # Controls browser's UI language
    options.add_argument("--disable-gpu")  # Best for console chrome
    options.add_argument("--ignore-certificate-errors")
    options.add_argument("--ignore-ssl-errors")
    options.add_argument("--disable-infobars")
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--disable-browser-side-navigation")
    options.add_argument("--disable-features=VizDisplayCompositor")
    options.add_argument("--disable-extensions")
    options.add_argument("--dns-prefetch-disable")
    if platform.system() == "Linux":
        options.add_argument("--no-sandbox")

    # Remove Chrome browser debugging info from the console (cosmetics only)
    options.add_experimental_option("excludeSwitches", ["enable-logging"])

    driver = webdriver.Chrome(
        service=Service(ChromeDriverManager(log_level=0).install()),
        options=options,
    )

    yield driver

    driver.quit()
    '''
